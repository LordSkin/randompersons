package kramnik.bartlomiej.randompersons.Presenter.Details;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;
import kramnik.bartlomiej.randompersons.View.ItemDetail.PersonViewHolder;

/**
 * Presenter for details view
 */

public class DetailPresenterImpl implements DetailPresenter {

    private Person person;
    PersonViewHolder view;
    private static DetailPresenter currentPresenter;

    /**
     * get detailPresnter for current view
     *
     * @return
     */
    public static DetailPresenter getDetailPresenter() {
        return currentPresenter;
    }

    public DetailPresenterImpl(int pos) {
        person = ListPresenterImpl.getPresenter().getPerson(pos);
    }

    public DetailPresenterImpl() {
    }

    @Override
    public void setPerson(int pos) {
        person = ListPresenterImpl.getPresenter().getPerson(pos);
    }

    @Override
    public void setView(PersonViewHolder personDetailViewCell) {
        this.view = personDetailViewCell;
    }

    @Override
    public void setCellCreditials() {
        if (view != null) view.setContent(person.getFullName());
    }

    @Override
    public int getPosition() {
        return ListPresenterImpl.getPresenter().getPersons().indexOf(person);
    }

    @Override
    public void setAsCurrentPresenter() {
        currentPresenter = this;
    }

    @Override
    public String getPersonDetails() {
        if (person != null)
            return person.getAddress() + "\n" + person.getCity() + "\n" + person.getEmail();
        else return "";
    }

    @Override
    public Person getPerson() {
        return person;
    }


}
