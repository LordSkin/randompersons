package kramnik.bartlomiej.randompersons.Presenter.Details;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.View.ItemDetail.PersonViewHolder;

/**
 * Interface for presenter menaging detailActivity/Fragment
 */

public interface DetailPresenter {

    public Person getPerson();

    public void setPerson(int pos);

    public void setView(PersonViewHolder view);

    public void setCellCreditials();

    public int getPosition();

    public void setAsCurrentPresenter();

    public String getPersonDetails();

}
