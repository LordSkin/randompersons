package kramnik.bartlomiej.randompersons.Presenter.List;

import java.util.ArrayList;
import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.Model.RequestHandler;
import kramnik.bartlomiej.randompersons.Model.DataService;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonsListView;
import rx.subjects.PublishSubject;

/**
 * Presenter for list view
 */

public class ListPresenterImpl implements RequestHandler, ListPresenter {

    private static ListPresenterImpl presenter;
    private PublishSubject<Void> observable;
    private List<Person> originalListOfPersons;
    private List<Person> filteredListOfPersons;
    private PersonsListView personListView;

    public ListPresenterImpl() {
        originalListOfPersons = new ArrayList<Person>();
        filteredListOfPersons = new ArrayList<Person>();
        observable = PublishSubject.create();
    }

    /**
     * @return one instance of presenter
     */
    public static ListPresenter getPresenter() {
        if (presenter == null) {
            presenter = new ListPresenterImpl();
        }
        return presenter;
    }

    @Override
    public void setListData(List<Person> persons) {
        this.originalListOfPersons = persons;
        filteredListOfPersons.addAll(persons);
        if (personListView != null) {
            personListView.personsListLoaded(persons);
            personListView.hideProgress();
        }
    }

    @Override
    public void showError() {
        if (personListView != null) {
            personListView.showError();
            personListView.hideProgress();
        }
    }

    private void requestData() {
        observable.onNext(null);
    }

    @Override
    public void setPersonsListView(PersonsListView personsListView) {
        this.personListView = personsListView;
        observable.subscribe(new DataService(this, personListView.getContext()));

    }

    @Override
    public List<Person> getPersons() {
        return filteredListOfPersons;
    }

    @Override
    public Person getPerson(int pos) {
        return filteredListOfPersons.get(pos);
    }

    @Override
    public void refreshPersons() {
        observable.onNext(null);
    }

    @Override
    public void getNewPersons() {
        if (originalListOfPersons.size() < 1) {
            if (personListView != null) personListView.showProgress();
            requestData();
        }
    }

    @Override
    public List<Person> filterList(String substring) {
        List<Person> result = filter(substring);
        if (personListView != null) personListView.refresh();
        return result;
    }

    @Override
    public PersonsListView getView() {
        return personListView;
    }

    /**
     * adds to filteredListOfPersons only elements containing substring
     *
     * @param substring
     * @return filteredListOfPersons
     */
    private List<Person> filter(String substring) {
        filteredListOfPersons.clear();
        for (Person p : originalListOfPersons) {
            if (p.getFullName().contains(substring)) {
                filteredListOfPersons.add(p);
            }
        }
        return filteredListOfPersons;
    }
}
