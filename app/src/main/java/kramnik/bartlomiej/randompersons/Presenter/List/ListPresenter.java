package kramnik.bartlomiej.randompersons.Presenter.List;

import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonsListView;

/**
 * interface for presenter to list view
 */

public interface ListPresenter {
    public void setPersonsListView(PersonsListView personsListView);

    public List<Person> getPersons();

    public Person getPerson(int pos);

    public void refreshPersons();

    public void getNewPersons();

    public List<Person> filterList(String substring);

    public PersonsListView getView();
}
