package kramnik.bartlomiej.randompersons.Model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Sending request and returning String response to dataService
 */

public class RequestSender {

    public final String URL = "https://randomuser.me/api/?results=20&nat=gb";
    private DataService dataService;
    private RequestQueue requestQueue;
    private Context context;

    public RequestSender(DataService dataService, Context context) {
        this.dataService = dataService;
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public void sendRequest() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dataService.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dataService.error(error);
            }
        });
        requestQueue.add(stringRequest);
        //requestQueue.start();
    }
}
