package kramnik.bartlomiej.randompersons.Model;

/**
 * interface for handling rsponses
 */

public interface RequestResponse {
    public void response(String json);

    public void error(Throwable e);
}
