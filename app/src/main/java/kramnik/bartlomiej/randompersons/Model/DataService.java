package kramnik.bartlomiej.randompersons.Model;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import rx.Observer;

/**
 * providing acces to sending request and interpreting response
 */


public class DataService implements RequestResponse, Observer<Void> {

    private RequestHandler presenter;
    private RequestSender requestSender;

    public DataService(RequestHandler presenter, Context context) {
        this.presenter = presenter;
        if (context != null) requestSender = new RequestSender(this, context);
    }

    /**
     * interpreting json response from request, sending  answer to handler
     *
     * @param json
     */
    @Override
    public void response(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("results");
            List<Person> listOfPersons = new ArrayList<Person>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject person = jsonArray.getJSONObject(i);
                JSONObject name = person.getJSONObject("name");
                JSONObject location = person.getJSONObject("location");
                listOfPersons.add(new Person(name.getString("title"), name.getString("first"), name.getString("last"), location.getString("street"), location.getString("city"), person.getString("email")));
            }
            presenter.setListData(listOfPersons);
        }
        catch (Exception e) {
            presenter.showError();
        }
    }

    @Override
    public void error(Throwable e) {
        presenter.showError();
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        presenter.showError();
    }

    @Override
    public void onNext(Void aVoid) {
        requestSender.sendRequest();
    }
}
