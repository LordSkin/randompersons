package kramnik.bartlomiej.randompersons.Model;

import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;

/**
 * interface to reacting with dataService
 */

public interface RequestHandler {
    public void setListData(List<Person> persons);

    public void showError();
}
