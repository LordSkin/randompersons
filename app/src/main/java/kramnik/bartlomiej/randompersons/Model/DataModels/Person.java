package kramnik.bartlomiej.randompersons.Model.DataModels;

/**
 * Data model of person
 */

public class Person {
    private String title;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String email;
    private boolean isFavorite;

    public Person(String title, String firstName, String lastName, String address, String city, String email) {
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.email = email;
        isFavorite = false;
    }

    public String getFullName() {
        return title + " " + firstName + " " + lastName;
    }

    public String getTitle() {
        return title;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    /**
     * switch if person is set as favorite
     *
     * @return if person is favorite after switching
     */
    public boolean switchFaworite() {
        isFavorite = !isFavorite;
        return isFavorite;
    }
}
