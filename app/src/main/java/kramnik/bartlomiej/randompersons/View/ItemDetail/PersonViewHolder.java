package kramnik.bartlomiej.randompersons.View.ItemDetail;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;
import kramnik.bartlomiej.randompersons.R;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonListActivity;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonsRecyclerViewAdapter;


/**
 * ViewHolder for RecycleView
 */

public class PersonViewHolder extends RecyclerView.ViewHolder implements PersonDetailViewCell, View.OnClickListener {
    private final View mView;
    private final TextView title;
    private final TextView content;
    public DetailPresenter presenter;

    public PersonViewHolder(View view, DetailPresenter presenter) {
        super(view);
        mView = view;
        title = (TextView) view.findViewById(R.id.id);
        content = (TextView) view.findViewById(R.id.content);
        this.presenter = presenter;
        this.presenter.setView(this);

        mView.setOnClickListener(this);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + content.getText() + "'";
    }

    @Override
    public void setTitle(String s) {
        title.setText(s);
    }

    @Override
    public void setContent(String s) {
        content.setText(s);
    }

    @Override
    public void onClick(View v) {
        if (PersonListActivity.BIG_SCREEN) {
            presenter.setAsCurrentPresenter();
            PersonDetailFragment fragment = new PersonDetailFragment();
            ((PersonListActivity) ListPresenterImpl.getPresenter().getView()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.person_detail_container, fragment)
                    .commit();
        }
        else {
            Context context = v.getContext();
            Intent intent = new Intent(context, PersonDetailActivity.class);
            presenter.setAsCurrentPresenter();
            context.startActivity(intent);
        }

    }
}
