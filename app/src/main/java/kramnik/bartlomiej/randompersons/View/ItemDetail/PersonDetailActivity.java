package kramnik.bartlomiej.randompersons.View.ItemDetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenter;
import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenterImpl;
import kramnik.bartlomiej.randompersons.R;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonListActivity;

/**
 * An activity representing a single Person detail screen. This
 * activity is only used narrow width devices.
 */
public class PersonDetailActivity extends AppCompatActivity {

    private DetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);

        presenter = DetailPresenterImpl.getDetailPresenter();

        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            PersonDetailFragment fragment = new PersonDetailFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.person_detail_container, fragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, PersonListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
