package kramnik.bartlomiej.randompersons.View.ItemDetail;

/**
 * interface to interaction with cell
 */

public interface PersonDetailViewCell {
    public void setTitle(String s);

    public void setContent(String s);
}
