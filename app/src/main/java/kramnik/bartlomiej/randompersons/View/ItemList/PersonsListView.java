package kramnik.bartlomiej.randompersons.View.ItemList;

import android.content.Context;

import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;

/**
 * Interface for View to list of persons
 */

public interface PersonsListView {

    public void showProgress();

    public void hideProgress();

    public void personsListLoaded(List<Person> persons);

    public void showError();

    public Context getContext();

    public void refresh();
}
