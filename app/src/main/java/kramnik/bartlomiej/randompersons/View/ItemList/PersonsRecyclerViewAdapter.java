package kramnik.bartlomiej.randompersons.View.ItemList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenterImpl;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;
import kramnik.bartlomiej.randompersons.R;
import kramnik.bartlomiej.randompersons.View.ItemDetail.PersonViewHolder;

/**
 * Adapter for RecycleView
 */

public class PersonsRecyclerViewAdapter extends RecyclerView.Adapter<PersonViewHolder> {

    private ListPresenter listPresenter;

    public PersonsRecyclerViewAdapter() {
        listPresenter = ListPresenterImpl.getPresenter();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.person_list_content, parent, false);
        return new PersonViewHolder(view, new DetailPresenterImpl());
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.presenter.setPerson(position);
        holder.presenter.setCellCreditials();
    }

    @Override
    public int getItemCount() {
        return listPresenter.getPersons().size();
    }
}
