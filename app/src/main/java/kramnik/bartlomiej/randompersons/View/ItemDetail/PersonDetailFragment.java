package kramnik.bartlomiej.randompersons.View.ItemDetail;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenter;
import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenterImpl;
import kramnik.bartlomiej.randompersons.R;
import kramnik.bartlomiej.randompersons.View.ItemDetail.PersonDetailActivity;
import kramnik.bartlomiej.randompersons.View.ItemList.PersonListActivity;

/**
 * A fragment representing a single Person detail screen.
 */
public class PersonDetailFragment extends Fragment implements View.OnClickListener {
    private DetailPresenter presenter;
    private TextView details;
    FloatingActionButton fab;

    public PersonDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = DetailPresenterImpl.getDetailPresenter();
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(presenter.getPerson().getFullName());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.person_detail, container, false);
        details = (TextView) rootView.findViewById(R.id.person_detail);
        details.setText(presenter.getPersonDetails());
        fab = (FloatingActionButton) rootView.findViewById(R.id.fabFavorite);
        if (presenter.getPerson().isFavorite()) fab.setImageResource(R.drawable.ic_start_yellow);
        else fab.setImageResource(R.drawable.ic_start_white);
        fab.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (presenter.getPerson().switchFaworite()) {
            fab.setImageResource(R.drawable.ic_start_yellow);
        }
        else {
            fab.setImageResource(R.drawable.ic_start_white);
        }
    }
}
