package kramnik.bartlomiej.randompersons.View.ItemList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.View.Dialogs.FilterDialog;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;
import kramnik.bartlomiej.randompersons.View.ItemDetail.PersonDetailActivity;
import kramnik.bartlomiej.randompersons.R;

import java.util.List;

/**
 * An activity representing a list of Persons. This activity
 * has different presentations for handset and tablet-size devices
 */
public class PersonListActivity extends AppCompatActivity implements PersonsListView {

    private ListPresenter presenter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private PersonsRecyclerViewAdapter adapter;
    public static boolean BIG_SCREEN = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        presenter = ListPresenterImpl.getPresenter();
        presenter.setPersonsListView(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterDialog dialog = new FilterDialog();
                dialog.show(getFragmentManager(), "Search");
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.person_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.person_detail_container) != null) BIG_SCREEN = true;

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        adapter = new PersonsRecyclerViewAdapter();
        recyclerView.setAdapter(new PersonsRecyclerViewAdapter());
        presenter.getNewPersons();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void personsListLoaded(List<Person> persons) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Something went wrong :(", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public void refresh() {
        adapter.notifyDataSetChanged();
    }
}
