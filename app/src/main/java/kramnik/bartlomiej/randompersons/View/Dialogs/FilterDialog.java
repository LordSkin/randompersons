package kramnik.bartlomiej.randompersons.View.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;
import kramnik.bartlomiej.randompersons.R;

/**
 * Dialog to filtering persons
 */

public class FilterDialog extends DialogFragment {

    private ListPresenter presenter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        presenter = ListPresenterImpl.getPresenter();
        View editView = inflater.inflate(R.layout.filter_persons_dialog, null);
        final EditText substring = (EditText) editView.findViewById(R.id.filterEdit);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Search persons")
                .setView(editView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presenter.filterList(substring.getText().toString());
                    }
                });
        return builder.create();

    }
}
