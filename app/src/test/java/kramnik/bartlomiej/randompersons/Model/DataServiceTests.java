package kramnik.bartlomiej.randompersons.Model;

import junit.framework.Assert;

import org.junit.Test;

import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;

/**
 * Created by A671811 on 2017-08-24.
 */

public class DataServiceTests {



    @Test
    public void jsonParsingTest() {
        DataService dataService = new DataService(new RequestHandler() {
            @Override
            public void setListData(List<Person> persons) {
                //OK
            }

            @Override
            public void showError() {

            }
        }, null);

        try {
            dataService.response(null);
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void jsonParsingTest2() {
        DataService dataService = new DataService(new RequestHandler() {
            @Override
            public void setListData(List<Person> persons) {
                Assert.fail();
            }

            @Override
            public void showError() {
                //OK
            }
        }, null);

        dataService.response(null);
    }


}
