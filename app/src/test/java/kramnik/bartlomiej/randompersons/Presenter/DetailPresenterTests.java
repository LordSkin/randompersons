package kramnik.bartlomiej.randompersons.Presenter;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenter;
import kramnik.bartlomiej.randompersons.Presenter.Details.DetailPresenterImpl;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;

/**
 * Created by A671811 on 2017-08-24.
 */

public class DetailPresenterTests {

    @Test
    public void gettingTest1() {
        Assert.assertNull(DetailPresenterImpl.getDetailPresenter());
    }

    @Test
    public void gettingTest2() {
        DetailPresenter presenter = new DetailPresenterImpl();
        presenter.setAsCurrentPresenter();
        Assert.assertNotNull(DetailPresenterImpl.getDetailPresenter());
    }

    @Test
    public void getPersonTest() {
        DetailPresenter presenter = new DetailPresenterImpl();
        Assert.assertEquals("", presenter.getPersonDetails());
    }

    @Test
    public void getPersonTest2() {
        DetailPresenter presenter = new DetailPresenterImpl();
        Assert.assertNull(presenter.getPerson());
    }

    @Test
    public void setPersonTest() {
        ListPresenterImpl listPresenter = (ListPresenterImpl) ListPresenterImpl.getPresenter();
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("", "", "", "", "", ""));
        persons.add(new Person("mr", "andrzej", "kowalski", "mila", "wrocalw", "aaa@wp.pl"));
        persons.add(new Person("miss", "anna", "anna", "asd", "dsa", "asd@wp.pl"));
        listPresenter.setListData(persons);


        try {
            DetailPresenter detailPresenter = new DetailPresenterImpl(0);
            Assert.assertNotNull(detailPresenter.getPerson());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void setPersonTest2() {
        ListPresenterImpl listPresenter = (ListPresenterImpl) ListPresenterImpl.getPresenter();
        List<Person> persons = new ArrayList<Person>();
        listPresenter.setListData(persons);


        try {
            DetailPresenter detailPresenter = new DetailPresenterImpl(0);
        }
        catch (IndexOutOfBoundsException e)
        {
            //OK
        }
        catch (Exception e) {
            Assert.fail();
        }
    }
}
