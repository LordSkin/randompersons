package kramnik.bartlomiej.randompersons.Presenter;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import kramnik.bartlomiej.randompersons.Model.DataModels.Person;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenter;
import kramnik.bartlomiej.randompersons.Presenter.List.ListPresenterImpl;

/**
 * Created by A671811 on 2017-08-24.
 */

public class ListPresenterTests {

    @Test
    public void getPrsentertest() {
        ListPresenter presenter = ListPresenterImpl.getPresenter();
        Assert.assertNotNull(presenter);
        Assert.assertNotNull(presenter.getPersons());
    }

    @Test
    public void emptyListGetTest() {
        ListPresenter presenter = ListPresenterImpl.getPresenter();
        try {
            presenter.getPerson(0);
        }
        catch (IndexOutOfBoundsException e) {
            //OK
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void listSetingTest() {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("", "", "", "", "", ""));
        persons.add(new Person("mr", "andrzej", "kowalski", "mila", "wrocalw", "aaa@wp.pl"));
        persons.add(new Person("miss", "anna", "anna", "asd", "dsa", "asd@wp.pl"));

        ListPresenterImpl presenter = new ListPresenterImpl();
        presenter.setListData(persons);
        try {
            List<Person> empty = presenter.filterList("xxxxxxxxxxxxxxxx");
            Assert.assertEquals(empty.size(), 0);
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void listSetingTest2() {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("", "", "", "", "", ""));
        persons.add(new Person("mr", "andrzej", "kowalski", "mila", "wrocalw", "aaa@wp.pl"));
        persons.add(new Person("miss", "anna", "anna", "asd", "dsa", "asd@wp.pl"));

        ListPresenterImpl presenter = new ListPresenterImpl();
        presenter.setListData(persons);
        try {
            List<Person> list = presenter.filterList("");
            Assert.assertEquals(3, list.size());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void listSetingTest3() {

        ListPresenterImpl presenter = new ListPresenterImpl();
        try {
            List<Person> list = presenter.filterList("");
            Assert.assertEquals(0, list.size());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void getPersonTest() {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("", "", "", "", "", ""));
        persons.add(new Person("mr", "andrzej", "kowalski", "mila", "wrocalw", "aaa@wp.pl"));
        persons.add(new Person("miss", "anna", "anna", "asd", "dsa", "asd@wp.pl"));
        ListPresenterImpl presenter = new ListPresenterImpl();
        presenter.setListData(persons);
        try {
            Assert.assertEquals("", presenter.getPerson(0).getTitle());
            Assert.assertEquals("andrzej", presenter.getPerson(1).getFirstName());
            Assert.assertEquals("asd@wp.pl", presenter.getPerson(2).getEmail());
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void getPersonTest2() {

        ListPresenterImpl presenter = new ListPresenterImpl();
        try {
            presenter.getPerson(0).getTitle();
        }
        catch (IndexOutOfBoundsException e) {
            //OK
        }
        catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void nullTest() {
        ListPresenter presenter = ListPresenterImpl.getPresenter();

        try {
            presenter.getNewPersons();
            presenter.refreshPersons();
            presenter.getPersons();
        }
        catch (Exception e) {
            Assert.fail();
        }
    }
}
